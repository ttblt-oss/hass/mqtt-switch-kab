"""Module defining entrypoint."""
import asyncio

from mqtt_switch_kab import device


def main():
    """Entrypoint function."""
    dev = device.MqttKab()
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
